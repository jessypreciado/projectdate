@extends('layouts.app')

@section('content')

    <form action="{{route('Datos.update',$datos->id)}}" method="post">
        {{method_field('PATCH')}}
        @csrf
        <div clas="container">
            <div class="form-group">
                <input type="text" class="form-control" name="nombre" value ="{{$datos->nombre}}" placeholder="Nombre">
            </div>

            <div class="form-group">
                <input type="text" class="form-control" name="apellidom" value ="{{$datos->apellidom}}" placeholder="Apellido Materno">
            </div>
            <div class="form-group">
                <input type="text" class="form-control" name="apellidop" value ="{{$datos->apellidop}}" placeholder="Apellido Paterno">
            </div>

            <div class="form-group">
                <input type="date" class="form-control" name="fechanacimiento"  value ="{{$datos->fechanacimiento}}" placeholder="Fecha Nacimiento">
            </div>

            <button type="submit" class="btn btn-primary">Guardar</button>
        </div>
    </form>
@endsection